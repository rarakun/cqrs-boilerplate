﻿using AppName.Core.Cqrs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.AppName.Helper;

namespace AppName.Test.Cqrs
{
    [TestClass]
    public class CommandDispatcherTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CommandDispatcher_Constructor_Null_ThrowsException()
        {
            AppName.Core.Cqrs.ICommandDispatcher commandDispatcher = new AppName.Core.Cqrs.CommandDispatcher(null);
        }
        [TestMethod]
        public void CommandDispatcher_Constructor_NotNull()
        {
            Mock<MockDependencyResolver> kernelMock = new Mock<MockDependencyResolver>(MockBehavior.Strict);
            Mock<ICommand> commandMock = new Mock<ICommand>(MockBehavior.Strict);

            AppName.Core.Cqrs.ICommandDispatcher commandDispatcher = new AppName.Core.Cqrs.CommandDispatcher(kernelMock.Object);

            Assert.IsNotNull(commandDispatcher);
        }
        [TestMethod]
        public void CommandDispatcher_DispatchShouldExecuteCommand()
        {
            Mock<MockDependencyResolver> kernelMock = new Mock<MockDependencyResolver>(MockBehavior.Strict);
            Mock<ICommand> commandMock = new Mock<ICommand>(MockBehavior.Strict);
            Mock<ICommandHandler<ICommand>> commandHandlerMock = new Mock<ICommandHandler<ICommand>>(MockBehavior.Strict);


            kernelMock.Setup(d => d.Get<ICommandHandler<ICommand>>()).Returns(commandHandlerMock.Object).Verifiable();
            commandHandlerMock.Setup(h => h.Execute(It.IsAny<ICommand>())).Verifiable();

            //kernelMock.Setup(k => k.Get<ICommand>()).Returns(commandMock.Object);
            AppName.Core.Cqrs.ICommandDispatcher commandDispatcher = new AppName.Core.Cqrs.CommandDispatcher(kernelMock.Object);

            commandDispatcher.Dispatch(commandMock.Object);

            kernelMock.Verify(k => k.Get<ICommandHandler<ICommand>>(), Times.Once);
            commandHandlerMock.Verify(k => k.Execute(It.IsAny<ICommand>()), Times.Once);
        }
    }
}
