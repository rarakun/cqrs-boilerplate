﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.AppName.Helper
{
    public static class AssertExtended
    {
        public static void HasProperty(object obj, string propertyName, string failMessage = null)
        {
            if (obj.GetType().GetProperty(propertyName) == null)
            {
                throw new Microsoft.VisualStudio.TestTools.UnitTesting.AssertFailedException(failMessage ?? string.Format("The object doesn't have a property named {0}", propertyName));
            }
        }
        public static void HasPropertyOfType(object obj, string propertyName, Type expectedType, string failMessage = null)
        {
            var property = obj.GetType().GetProperty(propertyName);

            if (!property.PropertyType.Equals(expectedType))
            {
                throw new Microsoft.VisualStudio.TestTools.UnitTesting.AssertFailedException(failMessage ?? string.Format("The object property's isn't of the type {0}", expectedType.Name));
            }
        }
        public static void HasMethod(object obj, string methodName, string failMessage = null)
        {
            if (obj.GetType().GetMethod(methodName) == null)
            {
                throw new Microsoft.VisualStudio.TestTools.UnitTesting.AssertFailedException(failMessage ?? string.Format("The object doesn't have a method named {0}", methodName));
            }
        }

        public static void HasProperty<Target>(string propertyName, string failMessage = null)
        {
            if (typeof(Target).GetProperty(propertyName) == null)
            {
                throw new Microsoft.VisualStudio.TestTools.UnitTesting.AssertFailedException(failMessage ?? string.Format("The object doesn't have a property named {0}", propertyName));
            }
        }
        public static void HasPropertyOfType<Target>(string propertyName, Type expectedType, string failMessage = null)
        {
            var property = typeof(Target).GetProperty(propertyName);

            if (!property.PropertyType.Equals(expectedType))
            {
                throw new Microsoft.VisualStudio.TestTools.UnitTesting.AssertFailedException(failMessage ?? string.Format("The object property's isn't of the type {0}", expectedType.Name));
            }
        }
        public static void HasMethod<Target>(string methodName, string failMessage = null)
        {
            if (typeof(Target).GetMethod(methodName) == null)
            {
                throw new Microsoft.VisualStudio.TestTools.UnitTesting.AssertFailedException(failMessage ?? string.Format("The object doesn't have a method named {0}", methodName));
            }
        }

    }
}
