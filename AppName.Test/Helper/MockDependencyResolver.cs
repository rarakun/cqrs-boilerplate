﻿using AppName.Core.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.AppName.Helper
{
    public class MockDependencyResolver : IDependencyResolver
    {
        public MockDependencyResolver()
        {

        }
        public virtual T Get<T>()
        {
            throw new InvalidOperationException("This method should be mocked");
        }
    }
}
