﻿using AppName.Core.Cqrs;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AppName.API.Controllers
{
    public abstract class BaseController : ApiController
    {
        [Inject]
        public ICommandDispatcher CommandDispatcher { get; set; }
        [Inject]
        public IQueryDispatcher QueryDispatcher { get; set; }
        // private helpers
        private static System.Collections.Concurrent.ConcurrentDictionary<Type, System.Reflection.PropertyInfo[]> TypePropertiesCache = new System.Collections.Concurrent.ConcurrentDictionary<Type, System.Reflection.PropertyInfo[]>();

        protected void Patch<TPatch, TEntity>(TPatch patch, TEntity entity)
            where TPatch : class
            where TEntity : class
        {
            System.Reflection.PropertyInfo[] properties = TypePropertiesCache.GetOrAdd(
                patch.GetType(),
                (type) => type.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public));

            foreach (System.Reflection.PropertyInfo prop in properties)
            {
                System.Reflection.PropertyInfo orjProp = entity.GetType().GetProperty(prop.Name);
                object value = prop.GetValue(patch);
                if (value != null)
                {
                    orjProp.SetValue(entity, value);
                }
            }
        }
    }
}