﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace AppName.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.Formatters.Insert(0, new WebApiContrib.Formatting.Jsonp.JsonpMediaTypeFormatter(new System.Net.Http.Formatting.JsonMediaTypeFormatter()));
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
