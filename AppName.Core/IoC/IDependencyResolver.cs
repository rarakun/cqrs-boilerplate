﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppName.Core.IoC
{
    public interface IDependencyResolver
    {
        T Get<T>();
    }
}
