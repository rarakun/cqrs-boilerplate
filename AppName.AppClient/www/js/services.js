angular.module('juggler.services', ['ngResource'])

.factory('Currencies', ['$resource',function($resource) {
	var baseUrl = "http://localhost:30284";
	;
  // Might use a resource here that returns a JSON array
	var currencyResource = $resource(baseUrl + "/api/Currency/:currencyId",
		{ callback: "JSON_CALLBACK" },
		{ get: { method: "JSONP" }});
  return {
    all: function(callback) {
		currencyResource.get(function (data)
		{
			callback(data.Currencies);
		});
    },
    remove: function(currency) {
		return $resource(baseUrl +"/" + currency.uid, {}, {
		  query: {method:'DELETE', isArray:false}
		});
    },
    get: function(currencyId, callback) {
		return currencyResource.get({currencyId: currencyId}, function (data)
		{
			callback(data.Currency);
		});
    }
  };
}]);
