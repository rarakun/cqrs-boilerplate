# cqrs-boilerplate


To replace the "AppName" folders and namespaces run the following powershell script:

```powershell
$originalName = "AppName"
$yourAppName = "YOURAPPNAME"
Get-ChildItem -Recurse | Rename-Item -NewName {$_.name -replace $originalName,$yourAppName}
Get-ChildItem -Recurse | ForEach-Object -Process {
  (Get-Content $_) -Replace $originalName, $yourAppName | Set-Content $_
}
```